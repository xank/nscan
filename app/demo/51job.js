const Jobs = require('./mongo').Jobs

module.exports = {
  // 网址和分页
  scan: {
    url: 'https://search.51job.com/list/020000,000000,0000,00,9,99,%2B,2,{{page}}.html?lang=c&stype=1&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&lonlat=0%2C0&radius=-1&ord_field=0&confirmdate=9&fromType=&dibiaoid=0&address=&line=&specialarea=00&from=&welfare=',
    page: 1
  },

  // 全局字段
  data: {
    city: '上海',
    origin: '51job'
  },

  // 列表页判断
  isList() {
    return this.$('#resultList .el')
  },

  // 列表获取 Url
  list() {
    return this.$list.find('a').attr('href')
  },

  // 详情页判断
  isDetail() {
    return this.$('.cname')
  },

  // 详情页数据
  detail() {
    const name = this.$('.cn h1').text()
    const salary = this.$('.cn strong').text()
    const companyName = this.$('.cname a').attr('title')

    const companyType = this.$('.ltype').text()
    const companyDesc = this.$('.tmsg').text()
    let address = this.$('.tBorderTop_box').eq(2).find('.bmsg').text()
    const insert = {
      name,
      salary,
      companyName,
      address,
      data: {
        companyType,
        companyDesc
      }
    }
    return insert
  },

  // 插入数据库
  insert(url, data) {
    console.log(data)
    Jobs.update(
      { url },
      { $set: data },
      {
        upsert: true
      }
    ).exec()
  }
}
